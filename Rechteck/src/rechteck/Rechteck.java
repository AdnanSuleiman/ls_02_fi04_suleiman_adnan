package rechteck;

public class Rechteck {
	
	private double a;
	private double b;
	
	public void setA(double a) {
		this.a = a;
	}
	
	public double getA() {
		return this.a;
	}
	
	public void setB(double b) {
		this.b = b;
	}
	
	public double getB() {
		return this.b;
	}
	
	public double getFlaeche() {
		return this.a * this.b;
	}
	
	public double getUmfang() {
		return 2 * (this.a + this.b);
	}
	
	public double getDiagonale() {
		return Math.sqrt(Math.pow(this.a, 2) + Math.pow(this.b, 2));
	}
}
