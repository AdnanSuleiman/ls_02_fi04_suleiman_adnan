package rechteck;

public class Testrechteck {
	
	public static void main(String[] args) {
		Rechteck rechteck = new Rechteck();
		
		rechteck.setA(10);
		rechteck.setB(20);
		
		System.out.println("a: " + rechteck.getA());
		System.out.println("b: " + rechteck.getB());
		System.out.println("Fl�che: " + rechteck.getFlaeche());
		System.out.println("Umfang: " + rechteck.getUmfang());
		System.out.println("Diagonale: " + rechteck.getDiagonale());
	}
}
