package main;

import ladung.Ladung;
import raumschiff.Raumschiff;

/**
 * 
 * @author Adnan Suleiman
 * 
 */

public class Main {
	
	/**
	 * Main-Methode des Programms
	 */
	
	public static void main(String[] args) {
		/**
		 * Setzten von Werten f�r das Klingonen Raumschiff
		 */
		
		Raumschiff r_klingonen = new Raumschiff();
		r_klingonen.setSchiffsname("IKS Hegh�ta");
		r_klingonen.setPhotonentorpedoAnzahl(1);
		r_klingonen.setEnergieversorgungInProzent(100);
		r_klingonen.setSchildeInProzent(100);
		r_klingonen.setHuelleInProzent(100);
		r_klingonen.setLebenserhaltungssystemeInProzent(100);
		r_klingonen.setAndroidenAnzahl(2);
		
		r_klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		r_klingonen.addLadung(new Ladung("Bat�leth Kinglonen Schwert", 200));
		
		/**
		 * Setzten von Werten f�r das Romunaler Raumschiff
		 */
		
		Raumschiff r_romulaner = new Raumschiff();
		r_romulaner.setSchiffsname("IRW Khazara");
		r_romulaner.setPhotonentorpedoAnzahl(2);
		r_romulaner.setEnergieversorgungInProzent(100);
		r_romulaner.setSchildeInProzent(100);
		r_romulaner.setHuelleInProzent(100);
		r_romulaner.setLebenserhaltungssystemeInProzent(100);
		r_romulaner.setAndroidenAnzahl(2);
		
		r_romulaner.addLadung(new Ladung("Borg Schrott", 5));
		r_romulaner.addLadung(new Ladung("Rote Materie", 2));
		r_romulaner.addLadung(new Ladung("Plasma-Waffe", 50));
		
		/**
		 * Setzten von Werten f�r das Vulkanier Raumschiff
		 */
		
		Raumschiff r_vulkanier = new Raumschiff();
		r_vulkanier.setSchiffsname("Ni�Var");
		r_vulkanier.setPhotonentorpedoAnzahl(0);
		r_vulkanier.setEnergieversorgungInProzent(80);
		r_vulkanier.setSchildeInProzent(80);
		r_vulkanier.setHuelleInProzent(50);
		r_vulkanier.setLebenserhaltungssystemeInProzent(100);
		r_vulkanier.setAndroidenAnzahl(5);
		
		r_vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		r_vulkanier.addLadung(new Ladung("Photonentorpedos", 3));
		
		/**
		 * Auszuf�hrende Methoden
		 */
		
		//1
		r_klingonen.photonentorpedosSchiessen(r_romulaner);
		
		//2
		r_romulaner.phaserkanoneSchiessen(r_klingonen);
		
		//3
		r_vulkanier.getBroadcastKommunikator().add("Gewalt ist nicht logisch");
		
		//4
		r_klingonen.zustandRaumschiff();
		r_klingonen.ladungsverzeichnisAusgeben();
		
		//5
		r_vulkanier.reparaturDurchfuehren(true, true, true, r_vulkanier.getAndroidenAnzahl());
		
		//6
		for(Ladung l : r_vulkanier.getLadungsverzeichnis()) {
			if(l.getBezeichnung().equalsIgnoreCase("Photonentorpedos")) {
				r_vulkanier.photonentorpedosLaden(l.getMenge());
			}
		}
		
		r_vulkanier.ladungsverzeichnisAufraeumen();
		
		//7
		r_klingonen.phaserkanoneSchiessen(r_romulaner);
		r_klingonen.phaserkanoneSchiessen(r_romulaner);
		
		//8
		r_klingonen.zustandRaumschiff();
		r_klingonen.ladungsverzeichnisAusgeben();
		
		r_romulaner.zustandRaumschiff();
		r_romulaner.ladungsverzeichnisAusgeben();
		
		r_vulkanier.zustandRaumschiff();
		r_vulkanier.ladungsverzeichnisAusgeben();
	}
}
