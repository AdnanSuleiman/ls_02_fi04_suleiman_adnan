package raumschiff;

import java.util.ArrayList;
import java.util.Random;

import ladung.Ladung;

/**
 * 
 * @author Adnan Suleiman
 * 
 */

public class Raumschiff {
	
	/**
	 * Bezeichnung des Namen des Schiffs
	 */
	
	private String schiffsname;
	
	/**
	 * Anzahl der vorhandenen Photonentorpedos auf dem Schiffs
	 */
	
	private int photonentorpedoAnzahl;
	
	/**
	 * Prozentualer Wert der Energieversorgung des Schiffs
	 */
	
	private int energieversorgungInProzent;
	
	/**
	 * Prozentualer Wert der Schilde des Schiffs 
	 */
	
	private int schildeInProzent;
	
	/**
	 * Prozentualer Wert der Schilde des Schiffes 
	 */
	
	private int huelleInProzent;
	
	/**
	 * Prozentualer Wert der Lebenserhaltungssysteme des Schiffes
	 */
	
	private int lebenserhaltungssystemeInProzent;
	
	/**
	 * Anzahl der vorhandenen Androiden auf dem Schiff
	 */
	
	private int androidenAnzahl;
	
	/**
	 * BroadcastKommunikator mit dem alle Schiffe kommunizieren k�nnen
	 */
	
	private static ArrayList<String> broadcastKommunikator = new ArrayList<>(); 
	
	/**
	 * Ladungsverzeichnis, in dem die Ladungsbezeichnung und die Ladungsmenge abgespeichert ist
	 */
	
	private ArrayList<Ladung> ladungsverzeichnis; 
	
	/**
	 * Konstruktor der Klasse Raumschiff mit standard Werten
	 */
	
	public Raumschiff() {
		this.schiffsname = "Schiff";
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.androidenAnzahl = 0;
		this.huelleInProzent = 0;
		
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}
	
	/**
	 * Konstruktor der Klasse Raumschiff mit Werten die �bergeben werden
	 */
	
	public Raumschiff(String schiffsname, int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl) {
		this.schiffsname = schiffsname;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}
	
	/**
	 * Erhalte den Namen des Schiffs
	 */

	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	/**
	 * Setze den Namen des Schiffs
	 */

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	/**
	 * Erhalte die Anzahl der Photonenthorpedos
	 */
	
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	
	/**
	 * Setzte die Anzahl der Photonenthorpedos
	 */
	
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	
	/**
	 * Erhalte den prozentualen Wert der Energieversorgung
	 */
	
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	
	/**
	 * Setze den prozentualen Wert der Energieversorgung
	 */
	
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	
	/**
	 * Erhalte den prozentualen Wert der Schilde
	 */

	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	
	/**
	 * Setze den prozentualen Wert der Schilde
	 */

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	
	/**
	 * Erhalte den prozentualen Wert der H�lle
	 */

	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	
	/**
	 * Setzte den prozentualen Wert der H�lle
	 */

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	
	/**
	 * Erhalte den prozentualen Wert der Lebenserhaltungssysteme
	 */

	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	
	/**
	 * Setzte den prozentualen Wert der Lebenserhaltungssysteme
	 */

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	
	/**
	 * Erhalte die Anzahl der Androiden
	 */

	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	
	/**
	 * Setzen der Anzahl der Androiden
	 */

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	
	/**
	 * Erhalten des BroadcastKommunikators
	 */

	public ArrayList<String> getBroadcastKommunikator() {
		return Raumschiff.broadcastKommunikator;
	}
	
	/**
	 * Setzten der BroadcastKommunikators
	 */

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}
	
	/**
	 * Erhalten des Ladungsverzeichnisses
	 */

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return this.ladungsverzeichnis;
	}
	
	/**
	 * Setzten des Ladungsverzeichnisses
	 */

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	/**
	 * Hinzuf�gen einer Ladung in das Ladungsverzeichnis
	 */
	
	public void addLadung(Ladung l) {
		this.ladungsverzeichnis.add(l);
	}
	
	/**
	 * Aufrufen des Zustands des Raumschiffes 
	 */
	
	public void zustandRaumschiff() {
		System.out.println("Raumschiff Zustand:");
		System.out.println("Photonentorpedo Anzah: " + getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung: " + getEnergieversorgungInProzent() + " %");
		System.out.println("Schilde: " + getEnergieversorgungInProzent() + " %");
		System.out.println("H�lle: " + getHuelleInProzent() + " %");
		System.out.println("Androiden Anzahl: " + getAndroidenAnzahl());
		System.out.println("");
	}
	
	/**
	 * Ausegeben des Ladungsverzeichnisses
	 */
	
	public void ladungsverzeichnisAusgeben() {
		System.out.println("Ladungsverzeichnis: ");
		
		for(Ladung l : this.ladungsverzeichnis) {
			System.out.println("Bezeichnung: " + l.getBezeichnung());
			System.out.println("Menge: " + l.getMenge());
		}
		
		System.out.println("");
	}
	
	/**
	 * Schie�en der Photonenthorpedos
	 */
	
	public void photonentorpedosSchiessen(Raumschiff r) {
		if(this.photonentorpedoAnzahl > 0) {
			System.out.println("Photonentorpedos abgeschossen");
			
			setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() - 1);
			
			treffer(r);
		} else {
			System.out.println("-=*Click*=-");
		}
	}
	
	/**
	 * Hinzuf�gen eines Treffers auf ein Raumschiff
	 */
	
	private void treffer(Raumschiff r) {
		System.out.println(r.getSchiffsname() + " wurde getroffen");
	}
	
	/**
	 * Schiessen der Phaserkanone auf ein Raumschiff
	 */
	
	public void phaserkanoneSchiessen(Raumschiff r) {
		if(this.energieversorgungInProzent > 50) {
			System.out.println("Phaserkanone abgeschossen");
			
			setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() - 1);
			
			treffer(r);
		} else {
			System.out.println("-=*Click*=-");
		}
	}
	
	/**
	 * Erhalten der logbuch Eintr�ge
	 */
	
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return Raumschiff.broadcastKommunikator;
	}
	
	/**
	 * Vermerken eines Treffers
	 */
	
	public void vermerkeTreffer() {
		setSchildeInProzent(getSchildeInProzent() - 50);
		
		if(getSchildeInProzent() <= 0) {
			setHuelleInProzent(getHuelleInProzent() - 50);
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50);
		}
		
		if(getHuelleInProzent() <= 0) {
			System.out.println("Die Lebenenserhaltungssysteme wurden vernichtet");
		}
	}
	
	/**
	 * Senden einer Nachricht an alle Raumschiffe
	 */
	
	public void nachrichtAnAlle(String message) {
		getBroadcastKommunikator().add(message);
		
		System.out.println(message);
	}
	
	/**
	 * Laden der Photonenthorpedos
	 */
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		if(getPhotonentorpedoAnzahl() <= 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			System.out.println("-=*Click*=-");
		}
		
		if(anzahlTorpedos > getPhotonentorpedoAnzahl()) {
			setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() + anzahlTorpedos);
			
		} else {
			for(Ladung l : getLadungsverzeichnis()) {
				if(l.getBezeichnung().equalsIgnoreCase("Photonentorpedos")) {
					while(l.getMenge() > 0) {
						l.setMenge(l.getMenge() - 1);
						setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() + 1); 
					}

					System.out.println("[" + anzahlTorpedos + "] Photonentorpedos eingesetzt");
				}
			}
		}
	}
	
	/**
	 * Aufr�umen des Ladungsverzeichnisses
	 */
	
	public void ladungsverzeichnisAufraeumen() {
			for(int i = 0; i < getLadungsverzeichnis().size(); i++) {
				if(getLadungsverzeichnis().get(i).getMenge() == 0) {
					getLadungsverzeichnis().remove(i);
			}
		}
	}
	
	
	/**
	 * Durchf�hren einer Reparatur an einem Raumschiff
	 */
	
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		int strukturen = 0;
		
		Random r = new Random();
		
		boolean schutzSchild = r.nextBoolean();
		if(schutzSchild) {
			strukturen += 1;
		}
		
		boolean energieVersorgung = r.nextBoolean();
		if(energieVersorgung) {
			strukturen += 1;
		}
		
		boolean schiffsHuelle = r.nextBoolean();
		if(schiffsHuelle) {
			strukturen += 1;
		}
		
		int value = r.nextInt(101);
		
		if(anzahlDroiden > getAndroidenAnzahl()) {
			setAndroidenAnzahl(getAndroidenAnzahl() + anzahlDroiden);
		}
		
		double shipStructures = ((value * (anzahlDroiden =- anzahlDroiden)) / (strukturen =- strukturen));
	}
}
