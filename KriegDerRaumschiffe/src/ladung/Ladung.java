package ladung;

/***
 * 
 * @author Adnan Suleiman
 * 
 */

public class Ladung {
	
	/***
	 * Name der Ladung
	 */
	
	private String bezeichnung;
	
	/***
	 * Menge der Ladung
	 */
	
	private int menge;
	
	/**
	 * Konstruktor f�r die Klasse Ladung
	 */
	
	public Ladung() {
		
	}
	
	/**
	 * Konstruktor f�r die Klasse Ladung mit Werten die �bergeben werden
	 */
	
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	/**
	 * Erhalte die Bezeichnng der Ladung
	 */
	
	public String getBezeichnung() {
		return this.bezeichnung;
	}
	
	/**
	 * Setzte die Beichnung der Ladung
	 */
	
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	/**
	 * Erhalte die Menge der Ladung
	 */
	
	public int getMenge() {
		return this.menge;
	}
	
	/**
	 * Setzte die Menge der Ladung
	 */
	
	public void setMenge(int menge) {
		this.menge = menge;
	}
}
